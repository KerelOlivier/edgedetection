﻿namespace EdgeDetectionForms {
    partial class EdgeDetection {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.StartButton = new System.Windows.Forms.Button();
            this.OriginalPb = new System.Windows.Forms.PictureBox();
            this.GreyscalePb = new System.Windows.Forms.PictureBox();
            this.SobelOperatorPb = new System.Windows.Forms.PictureBox();
            this.CannyEdgePb = new System.Windows.Forms.PictureBox();
            this.SelectButton = new System.Windows.Forms.Button();
            this.ImageLocationTb = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.OriginalPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GreyscalePb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SobelOperatorPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CannyEdgePb)).BeginInit();
            this.SuspendLayout();
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(445, 408);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(75, 23);
            this.StartButton.TabIndex = 0;
            this.StartButton.Text = "Start";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.Start_Click);
            // 
            // OriginalPb
            // 
            this.OriginalPb.BackColor = System.Drawing.SystemColors.ControlDark;
            this.OriginalPb.Location = new System.Drawing.Point(12, 12);
            this.OriginalPb.Name = "OriginalPb";
            this.OriginalPb.Size = new System.Drawing.Size(132, 126);
            this.OriginalPb.TabIndex = 1;
            this.OriginalPb.TabStop = false;
            // 
            // GreyscalePb
            // 
            this.GreyscalePb.BackColor = System.Drawing.SystemColors.ControlDark;
            this.GreyscalePb.Location = new System.Drawing.Point(12, 144);
            this.GreyscalePb.Name = "GreyscalePb";
            this.GreyscalePb.Size = new System.Drawing.Size(132, 126);
            this.GreyscalePb.TabIndex = 2;
            this.GreyscalePb.TabStop = false;
            // 
            // SobelOperatorPb
            // 
            this.SobelOperatorPb.BackColor = System.Drawing.SystemColors.ControlDark;
            this.SobelOperatorPb.Location = new System.Drawing.Point(12, 276);
            this.SobelOperatorPb.Name = "SobelOperatorPb";
            this.SobelOperatorPb.Size = new System.Drawing.Size(132, 126);
            this.SobelOperatorPb.TabIndex = 3;
            this.SobelOperatorPb.TabStop = false;
            // 
            // CannyEdgePb
            // 
            this.CannyEdgePb.BackColor = System.Drawing.SystemColors.ControlDark;
            this.CannyEdgePb.Location = new System.Drawing.Point(150, 12);
            this.CannyEdgePb.Name = "CannyEdgePb";
            this.CannyEdgePb.Size = new System.Drawing.Size(370, 390);
            this.CannyEdgePb.TabIndex = 4;
            this.CannyEdgePb.TabStop = false;
            // 
            // SelectButton
            // 
            this.SelectButton.Location = new System.Drawing.Point(364, 408);
            this.SelectButton.Name = "SelectButton";
            this.SelectButton.Size = new System.Drawing.Size(75, 23);
            this.SelectButton.TabIndex = 5;
            this.SelectButton.Text = "Select";
            this.SelectButton.UseVisualStyleBackColor = true;
            this.SelectButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // ImageLocationTb
            // 
            this.ImageLocationTb.Location = new System.Drawing.Point(12, 410);
            this.ImageLocationTb.Name = "ImageLocationTb";
            this.ImageLocationTb.Size = new System.Drawing.Size(346, 20);
            this.ImageLocationTb.TabIndex = 6;
            // 
            // EdgeDetection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 443);
            this.Controls.Add(this.ImageLocationTb);
            this.Controls.Add(this.SelectButton);
            this.Controls.Add(this.CannyEdgePb);
            this.Controls.Add(this.SobelOperatorPb);
            this.Controls.Add(this.GreyscalePb);
            this.Controls.Add(this.OriginalPb);
            this.Controls.Add(this.StartButton);
            this.Name = "EdgeDetection";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.OriginalPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GreyscalePb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SobelOperatorPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CannyEdgePb)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.PictureBox OriginalPb;
        private System.Windows.Forms.PictureBox GreyscalePb;
        private System.Windows.Forms.PictureBox SobelOperatorPb;
        private System.Windows.Forms.PictureBox CannyEdgePb;
        private System.Windows.Forms.Button SelectButton;
        private System.Windows.Forms.TextBox ImageLocationTb;
    }
}

